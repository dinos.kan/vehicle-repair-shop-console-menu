package persistence;

import domain.Part;

import static org.junit.jupiter.api.Assertions.*;

class PartDAOImplTest {

    void allowEntryOfPart() {
        Part part = new Part(92, "airbag", 68.8, 15);
        assertEquals(91, part.getPartId());
    }

}