package persistence;

import domain.Part;

import java.util.List;

public interface PartDAO {
    void insertPart(long partId, String partType, double partCost, long repairId);

    boolean deletePartFromList(long partId);

    boolean updatePart(Part partToUpdate);

    List<Part> findAllParts();

    Part findPartById(long partId);

    Part findPartByType(String partType);

    Part findPartByCost(int partCost);

    List<Part> findRepairParts(long repairId);
}
