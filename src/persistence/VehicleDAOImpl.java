package persistence;

import domain.Vehicle;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleDAOImpl implements VehicleDAO {

    private static ArrayList<Vehicle> vehiclesList = new ArrayList();

    private void addVehicleToList(Vehicle vehicle){
        vehiclesList.add(vehicle);
    }

    @Override
    public void insertVehicle(long vehicleId,
                              String brand,
                              String model,
                              LocalDate creationDate,
                              String plateNumber,
                              String color,
                              int price,
                              long ownerId){
        Vehicle vehicle = new Vehicle(vehicleId, brand, model,
                creationDate, plateNumber, color, price, ownerId);

        addVehicleToList(vehicle);
    }

    @Override
    public boolean deleteVehicleFromList(long id){
        return vehiclesList.removeIf(vehicle -> vehicle.getVehicleId() == id);
    }

    @Override
    public boolean updateVehicle(Vehicle vehicleToUpdate){
        for(Vehicle vehicle:vehiclesList){
            if(vehicle.getVehicleId() == vehicleToUpdate.getVehicleId()){
                updateFields(vehicleToUpdate, vehicle);
                return true;
            }
        }
        return false;
    }

    private void updateFields(Vehicle vehicleToUpdate, Vehicle vehicle) {
        vehicle.setBrand(vehicleToUpdate.getBrand());
        vehicle.setModel(vehicleToUpdate.getModel());
        vehicle.setColor(vehicleToUpdate.getColor());
        vehicle.setPlateNumber(vehicleToUpdate.getPlateNumber());
        vehicle.setCreationDate(vehicleToUpdate.getCreationDate());
        vehicle.setPrice(vehicleToUpdate.getPrice());
    }

    @Override
    public List<Vehicle> findAllVehicles(){
        return vehiclesList;
    }

    @Override
    public Vehicle findVehicleById(long vehicleId){
        return vehiclesList.stream()
                .filter(vehicle -> vehicle.getVehicleId() == vehicleId)
                .findAny()
                .orElse(null);
    }

    @Override
    public List<Vehicle> findVehicleByData(String givenData) {

        List<Vehicle> vehicles = new ArrayList<>();
        for (Vehicle vehicle: vehiclesList) {
            if (vehicle.getPlateNumber().equals(givenData)){
                vehicles.add(vehicle);
            } else if (vehicle.getModel().equals(givenData)){
                vehicles.add(vehicle);
            } else if (vehicle.getColor().equals(givenData)){
                vehicles.add(vehicle);
            } else if (vehicle.getBrand().equals(givenData)){
                vehicles.add(vehicle);
            }
        }
        return vehicles;
    }

    @Override
    public List<Vehicle> findVehiclesByUserId(long userId){
        return vehiclesList.stream()
                .filter(vehicle -> vehicle.getOwnerId() == userId)
                .collect(Collectors.toList());
    }
}
