package persistence;

import domain.Part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class PartDAOImpl implements PartDAO {

    private static List<Part> partsList = new ArrayList();

    private void addPartToList(Part part){
        partsList.add(part);
    }

    @Override
    public void insertPart(long partId,
                           String partType,
                           double partCost,
                           long repairId){
        Part part = new Part(partId, partType, partCost, repairId);
        addPartToList(part);
    }

    @Override
    public boolean deletePartFromList(long partId){
        return partsList.removeIf(part -> part.getPartId() == partId);
    }

    @Override
    public boolean updatePart(Part partToUpdate){
        for (Part part:partsList){
            if(part.getPartId() == partToUpdate.getPartId()){
                updateFields(partToUpdate, part);
                return true;
            }
        }
        return false;
    }

    private void updateFields(Part partToUpdate, Part part) {
        part.setPartCost(partToUpdate.getPartCost());
        part.setPartType(partToUpdate.getPartType());
        part.setRepairId(partToUpdate.getRepairId());
    }

    @Override
    public List<Part> findAllParts(){
        return partsList;
    }

    @Override
    public Part findPartById(long partId){
        return partsList.stream()
                .filter(part -> part.getPartId() == partId)
                .findAny()
                .orElse(null);
    }

    @Override
    public Part findPartByType(String partType){
        return partsList.stream()
                .filter(part -> part.getPartType().equals(partType))
                .findAny()
                .orElse(null);
    }

    @Override
    public Part findPartByCost(int partCost){
        return partsList.stream()
                .filter(part -> part.getPartCost() == partCost)
                .findAny()
                .orElse(null);
    }

    @Override
    public List<Part> findRepairParts(long repairId){

        return partsList.stream()
                .filter(part -> part.getRepairId() == repairId)
                .collect(Collectors.toList());
    }

    public double calcRepairPartsCost(long repairId){
        double partsCost = 0.0;
        for (Part part: partsList){
            if (repairId == part.getRepairId()){
                partsCost = partsCost + part.getPartCost();
            }
        }
        return partsCost;
    }
}