package persistence;

import domain.Repair;
import utils.RepairStatus;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RepairDAOImpl implements RepairDAO {

    private static ArrayList<Repair> repairsList = new ArrayList();

    private void addRepairToList(Repair repair){
        repairsList.add(repair);
    }

    @Override
    public void insertRepair(long repairId,
                             LocalDate repairDate,
                             RepairStatus repairStatus,
                             long vehicleId,
                             double repairCost) {
        Repair repair = new Repair(repairId, repairDate, repairStatus, vehicleId, repairCost);
        addRepairToList(repair);
    }

    @Override
    public boolean deleteRepairFromList(long id){
        return repairsList.removeIf(repair -> repair.getRepairId() == id);
    }

    @Override
    public boolean updateRepair(Repair repairToUpdate){
        for(Repair repair:repairsList){
            if(repair.getRepairId() == repairToUpdate.getRepairId()){
                updateFields(repairToUpdate, repair);
                return true;
            }
        }
        return false;
    }

    private void updateFields(Repair repairToUpdate, Repair repair) {
        repair.setRepairCost(repairToUpdate.getRepairCost());
        repair.setRepairDate(repairToUpdate.getRepairDate());
        repair.setRepairStatus(repairToUpdate.getRepairStatus());
    }

    @Override
    public List<Repair> findAllRepairs(){
        return repairsList;
    }

    @Override
    public Repair findRepairById(long repairId){
        return repairsList.stream()
                .filter(repair -> repair.getRepairId() == repairId)
                .findAny()
                .orElse(null);
    }

    @Override
    public List<Repair> findVehicleRepairs(long vehicleId) {

        return repairsList.stream()
                .filter(repair -> repair.getVehicleId() == vehicleId)
                .collect(Collectors.toList());
    }

    @Override
    public double calcRepairCost(long repairId){
        double repairCost = 0.0;
        for (Repair repair: repairsList){
            if (repairId == repair.getRepairId()){
                repairCost = repairCost + repair.getRepairCost();
            }
        }
        return repairCost;
    }

}
