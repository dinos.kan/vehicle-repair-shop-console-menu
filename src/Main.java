import domain.Part;
import domain.Repair;
import domain.User;
import domain.Vehicle;
import services.PartService;
import services.RepairService;
import services.UserService;
import services.VehicleService;

import java.time.LocalDate;
import java.util.List;

import static utils.RepairStatus.*;
import static utils.UserType.ADMINISTRATOR;
import static utils.UserType.USER;

public class Main {

    public static void main(String[] args) {

        UserService userService = new UserService();
        VehicleService vehicleService = new VehicleService();
        RepairService repairService = new RepairService();
        PartService partService = new PartService();


        //DATES
        LocalDate date1 = LocalDate.of(2015, 12, 20);
        LocalDate date2 = LocalDate.of(2010, 10, 10);
        LocalDate date3 = LocalDate.of(2005, 8, 15);
        LocalDate date4 = LocalDate.of(2018, 11, 17);
        LocalDate date5 = LocalDate.of(1999, 4, 13);
        LocalDate date6 = LocalDate.of(2007, 6, 15);
        LocalDate date7 = LocalDate.of(2006, 8, 17);


        //USERS
        userService.insertUser(1, "dinos@dinos.com", "dinos", "Konstantinos", "Kanakakis",
                "Egaleo", 160010264, USER);
        userService.insertUser(2, "makis@makis.com", "makis", "Prodromos", "Sakis",
                "Peristeri", 987654321, ADMINISTRATOR);
        userService.insertUser(3, "nana@nana.com", "nana", "Anastasia", "Kanakaki",
                "Chaidari", 567891234, USER);
        userService.insertUser(4, "lakis@lakis.gr", "lakis", "Nikos", "Panagoulis",
                "Egaleo", 111222333, USER);
        User userToUpdate = new User(4, "takis@takis.com", "takis", "Panagiotis", "Tsoukalas",
                "Pireas", 123456789, ADMINISTRATOR);


        //VEHICLES
        vehicleService.insertVehicle(1, "Alfa Romeo", "156", "ZZZ9008", date1, "white", 5000, 1);
        vehicleService.insertVehicle(2, "Audi", "A1", "KMT1718", date2, "red", 10500, 2);
        vehicleService.insertVehicle(3, "Audi", "TT", "ZKN7890", date3, "black", 15000, 3);
        vehicleService.insertVehicle(4, "BMW", "Z4", "KAM1711", date4, "white", 28900, 4);


        //REPAIRS
        repairService.insertRepair(10, date5, FINISHED, 1, 60);
        repairService.insertRepair(11, date6, IN_PROGRESS, 2, 30.2);
        repairService.insertRepair(12, date6, SCHEDULED, 1, 25.7);
        repairService.insertRepair(13, date7, IN_PROGRESS, 3, 15);


        //PARTS
        partService.insertPart(91, "engine", 300, 10);
        partService.insertPart(92, "wheel", 90.5, 10);
        partService.insertPart(93, "sensors", 45.4, 11);
        partService.insertPart(94, "airbag", 65, 13);


        //Methods calling
        List<User> allUsers = userService.findAllUsers();
        System.out.println("findAllUsers:         " + allUsers);

        List<Vehicle> allVehicles = vehicleService.findAllVehicles();
        System.out.println("findAllVehicles:      " + allVehicles);

        List<Repair> allRepairs = repairService.findAllRepairs();
        System.out.println("findAllRepairs:       " + allRepairs);

        List<Part> allParts = partService.findAllParts();
        System.out.println("findAllParts:         " + allParts);

        boolean b = userService.deleteUserFromList(2);
        System.out.println("deleteUserFromList(2):" + b);

        boolean bool = userService.updateUser(userToUpdate);
        System.out.println("updateUser:           " + bool);

        List<User> allUsersUpdated = userService.findAllUsers();
        System.out.println("findAllUsers:         " + allUsersUpdated);

        List<User> users = userService.findUserByData("Konstantinos");
        System.out.println("findUserByData:       " + users.toString());

        User user = userService.findUserById(3);
        System.out.println("findUserById:         " + user);

        Repair repair = repairService.findRepairById(10);
        System.out.println("findRepairById:       " + repair);

        Vehicle vehicle = vehicleService.findVehicleById(1);
        System.out.println("findVehicleById:      " + vehicle);

        List<Vehicle> vehicle1 = vehicleService.findVehicleByData("white");
        System.out.println("findVehicleByData:    " + vehicle1.toString());

        List<Repair> vehicleRepairs = repairService.findVehicleRepairs(1);
        System.out.println("findVehicleRepairs:   " + vehicleRepairs.toString());

        List<Repair> userRepairs = vehicleService.findUserRepairs(1);
        System.out.println("findUserRepairs:      " + userRepairs.toString());

        List<Part> parts = partService.findRepairParts(10);
        System.out.println("findRepairParts:      " + parts.toString());

        List<Part> parts1 = repairService.findRepairParts(12);
        System.out.println("findRepairParts:      " + parts1.toString());

        List<Vehicle> vehicles = vehicleService.findUserVehicles(2);
        System.out.println("findUserVehicles:     " + vehicles);

        double totalCost = repairService.calcTotalCost(10);
        System.out.println("calcTotalCost:        " + totalCost);

    }
}