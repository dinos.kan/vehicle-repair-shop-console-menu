package utils;

public enum RepairStatus {
    SCHEDULED,
    IN_PROGRESS,
    FINISHED,
    CANCELED;
}
